package org.ssh.math.algebra;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Emre Elmas
 */
public class ComplexRingTest {
    private ComplexRing complexRing = new ComplexRing();
    private ComplexNumber complexNumber1 = new ComplexNumber(-1d, 2d);
    private ComplexNumber complexNumber2 = new ComplexNumber(15d, 20d);
    private ComplexNumber complexNumber3 = new ComplexNumber(-5d, -10d);

    private ComplexNumber complexNumberZero = new ComplexNumber(0d, 0d);


    @Test
    public void add() throws Exception {
        //Associativity: (A+B)+C = A+(B+C)
        assertEquals(complexRing.add(complexRing.add(complexNumber1, complexNumber2), complexNumberZero).getReal(),
                complexRing.add(complexNumber1, complexRing.add(complexNumber2, complexNumberZero)).getReal(), 0);
        assertEquals(complexRing.add(complexRing.add(complexNumber1, complexNumber2), complexNumberZero).getComplex(),
                complexRing.add(complexNumber1, complexRing.add(complexNumber2, complexNumberZero)).getComplex(), 0);
        //Identity
        assertEquals(complexRing.add(complexNumber1, complexNumberZero).getReal(),
                complexRing.add(complexNumberZero, complexNumber1).getReal(), 0);
        assertEquals(complexRing.add(complexNumber1, complexNumberZero).getComplex(),
                complexRing.add(complexNumberZero, complexNumber1).getComplex(), 0);
        //Distributivity
        assertEquals(complexRing.multiply(complexNumber1, complexRing.add(complexNumber2, complexNumber3)).getReal(),
                complexRing.add(complexRing.multiply(complexNumber1, complexNumber2), complexRing.multiply(complexNumber1, complexNumber3)).getReal(), 0);
        assertEquals(complexRing.multiply(complexNumber1, complexRing.add(complexNumber2, complexNumber3)).getComplex(),
                complexRing.add(complexRing.multiply(complexNumber1, complexNumber2), complexRing.multiply(complexNumber1, complexNumber3)).getComplex(), 0);
    }

    @Test
    public void getAdditiveInverse() throws Exception {
        // Additive inverse is 0
        assertTrue(complexRing.getAdditiveInverse(complexNumberZero).getReal() == 0d
                || complexRing.getAdditiveInverse(complexNumberZero).getReal() == -0d);
        assertTrue(complexRing.getAdditiveInverse(complexNumberZero).getComplex() == 0d
                || complexRing.getAdditiveInverse(complexNumberZero).getComplex() == -0d);

        // a+(-a) = 0
        assertEquals(complexRing.add(complexRing.getAdditiveInverse(complexNumber1), complexNumber1).getComplex(), 0d, 0);
        assertEquals(complexRing.add(complexRing.getAdditiveInverse(complexNumber1), complexNumber1).getReal(), 0d, 0);

    }

    @Test
    public void multiply() throws Exception {
        //Associativity
        assertEquals(complexRing.multiply(complexRing.multiply(complexNumber1, complexNumber2), complexNumber3).getReal(),
                complexRing.multiply(complexNumber1, complexRing.multiply(complexNumber2, complexNumber3)).getReal(), 0);
        assertEquals(complexRing.multiply(complexRing.multiply(complexNumber1, complexNumber2), complexNumber3).getComplex(),
                complexRing.multiply(complexNumber1, complexRing.multiply(complexNumber2, complexNumber3)).getComplex(), 0);
        //Identity
        assertEquals(complexRing.multiply(complexNumber1, complexNumberZero).getReal(),
                complexRing.multiply(complexNumberZero, complexNumber1).getReal(), 0);
        assertEquals(complexRing.multiply(complexNumber1, complexNumberZero).getComplex(),
                complexRing.multiply(complexNumberZero, complexNumber1).getComplex(), 0);

    }

    @Test
    public void getMultiplicativeInverse() throws Exception {
        assertEquals(complexRing.multiply(complexNumber1, complexRing.getMultiplicativeInverse(complexNumber1)).getComplex() +
                complexRing.multiply(complexNumber1, complexRing.getMultiplicativeInverse(complexNumber1)).getReal(), 1d, 0);
    }

}